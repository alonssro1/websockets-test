const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 8080 });

const rooms = {};

wss.on('connection', (ws) => {
  ws.on('message', (message) => {
    const data = JSON.parse(message);
    
    if (data.type === 'join') {
      const { room, user } = data;
      if (!rooms[room]) {
        rooms[room] = [];
      }
      rooms[room].push({ ws, user });
      ws.room = room;
      ws.user = user;
      ws.send(JSON.stringify({ user: 'Bot', text: `Chat room: ${room}` }));
    } else if (data.type === 'message') {
      const { room, text } = data;
      const roomUsers = rooms[room] || [];
      roomUsers.forEach(client => {
        if (client.ws.readyState === WebSocket.OPEN) {
          client.ws.send(JSON.stringify({ user: data.user, text }));
        }
      });
    }
  });

  ws.on('close', () => {
    const room = ws.room;
    if (room) {
      rooms[room] = rooms[room].filter(client => client.ws !== ws);
    }
  });
});

console.log('WebSocket server is running on ws://localhost:8080');
