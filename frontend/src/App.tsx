import React, { useEffect, useState } from "react";
import { Filter, ToDo } from "./types";
import { ToDoList } from "./components";

function App() {
  const [todos, setTodos] = useState<ToDo[]>([]);
  const [newTodo, setNewTodo] = useState<string>('');
  const [filter, setFilter] = useState<Filter>('all');

  useEffect(() => {
    const savedTodos = localStorage.getItem('todos');
    if (savedTodos) {
      setTodos(JSON.parse(savedTodos));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem('todos', JSON.stringify(todos));
  }, [todos]);

  const addTodo = () => {
    if (newTodo.trim() !== '') {
      setTodos([...todos, { text: newTodo, completed: false }]);
      setNewTodo('');
    }
  };

  const getFilteredTodos = (): ToDo[] => {
    switch (filter) {
      case 'active':
        return todos.filter(todo => !todo.completed);
      case 'completed':
        return todos.filter(todo => todo.completed);
      case 'all':
      default:
        return todos;
    }
  };

  return (
    <div className="flex flex-col items-center justify-center min-h-screen bg-gray-100">
      <div className="App bg-white p-4 rounded-lg shadow-md w-full max-w-md">
        <h1 className="text-2xl font-bold mb-4">To-Do List</h1>
        <div className="flex">
          <input 
            onKeyDown={e => e.key === 'Enter' && addTodo()}
            type="text" 
            value={newTodo} 
            onChange={(e) => setNewTodo(e.target.value)} 
            placeholder="Add a new task"
            className="p-2 border border-gray-300 rounded mr-2 w-full"
          />
          <button 
            onClick={addTodo}
            className="p-2 bg-blue-500 text-white rounded hover:bg-blue-600"
          >
            Add
          </button>
        </div>
        <div className="flex mt-4">
          <button 
            onClick={() => setFilter('all')}
            className={`p-2 rounded ${filter === 'all' ? 'bg-blue-500 text-white' : 'bg-gray-200 text-gray-800'} hover:bg-blue-600`}
          >
            All
          </button>
          <button 
            onClick={() => setFilter('active')}
            className={`p-2 rounded ${filter === 'active' ? 'bg-blue-500 text-white' : 'bg-gray-200 text-gray-800'} hover:bg-blue-600 ml-2`}
          >
            Active
          </button>
          <button 
            onClick={() => setFilter('completed')}
            className={`p-2 rounded ${filter === 'completed' ? 'bg-blue-500 text-white' : 'bg-gray-200 text-gray-800'} hover:bg-blue-600 ml-2`}
          >
            Completed
          </button>
        </div>
        <ToDoList todos={getFilteredTodos()} setTodos={setTodos} />
      </div>
    </div>
  );
}

export default App;
